# Brucellosis

Holt, H.R., 2020. Towards progressive control of brucellosis in Punjab State (India): epidemiology and simulation of control strategies (Doctoral dissertation, University of London Royal Veterinary College).

## master.brucellosis

Within-herd stochastic transmission model for bovine brucellosis

## events

Events matrix - which animals transition compartments during each model 'event'

---

# within_herd

Files required to run within-herd control scenarios. 

## within.herd.control.R

Control scenario for bovine brucellosis - can turn on/off vaccination of adults at the start of the programme, vaccination of replacement calves and screening of replacements. 

## settings.R

Model settings for the control scenario - can turn on/off different control options and change % vaccination coverage. Can also change transmission parameters for scenario analysis

## sim.herds

1000 herds run for 75-years to 'endemic' stability

Parameterised with data from:

Holt, H.R., Bedi, J.S., Kaur, P., Mangtani, P., Sharma, N.S., Gill, J.P.S., Singh, Y., Kumar, R., Kaur, M., McGiven, J. and Guitian, J., 2021. 
Epidemiology of brucellosis in cattle and dairy farmers of rural Ludhiana, Punjab. PLoS neglected tropical diseases, 15(3), p.e0009102.



